'use strict';

var request = require('superagent');

var baseUrl = 'https://mobile.sofascore.com/mobile/v4';
var userAgent = 'Dalvik/2.1.0 (Linux; U; Android 5.1; Google Nexus 4 - 5.1.0 - API 22 - 768x1280 Build/LMY47D)';

var req = function (path, cb) {
    request
        .get(baseUrl + path)
        .set({
            'User-Agent': userAgent
        })
        .end(function (err, res) {
            if (err) return cb(err);

            cb(null, res.body);
        })
};

module.exports = {
    teams: function (sport, cb) {
        req('/sport/' + sport + '/teams', cb)
    },
    // Teams
    teamDetails: function (teamId, cb) {
        req('/team/' + teamId + '/details', cb)
    },
    teamEvents: function (teamId, cb) {
        req('/team/' + teamId + '/events', cb)
    },
    teamLastNext: function (teamId, cb) {
        req('/team/' + teamId + '/lastnext', cb)
    },
    teamLogo: function (teamId, cb) {
        req('/team/' + teamId + '/logo', cb)
    },
    teamTournaments: function (teamId, cb) {
        req('/team/' + teamId + '/tournaments', cb)
    },
    teamPlayers: function (teamId, cb) {
        req('/team/' + teamId + '/players', cb)
    },
    teamPerformance: function (teamId, cb) {
        req('/team/' + teamId + '/performance', cb)
    },
    // Events
    eventDetails: function (eventId, cb) {
        req('/event/' + eventId + '/details', cb)
    },
    eventLineups: function (eventId, cb) {
        req('/event/' + eventId + '/lineups', cb)
    },
    eventHead2Head: function (eventId, cb) {
        req('/event/' + eventId + '/head2head', cb)
    },
    eventIncidents: function (eventId, cb) {
        req('/event/' + eventId + '/incidents', cb)
    },
    eventStatistics: function (eventId, cb) {
        req('/event/' + eventId + '/statistics', cb)
    },
    // Standings
    tournamentStandings: function (opts, cb) {
        var tournamentId = opts.tournamentId || 0;
        var sessionId = opts.sessionId || 0;

        req('/tournament/' + tournamentId + '/season/' + sessionId + '/standings', cb)
    },
    // Players
    playerDetails: function (playerId, cb) {
        req('/player/' + playerId + '/details', cb)
    },
    playerStatistics: function (playerId, cb) {
        req('/player/' + playerId + '/statistics', cb)
    },
    playerEventStatistics: function (eventId, playerId, cb) {
        req('/event/' + eventId + '/player/' + playerId + '/statistics', cb)
    }
};